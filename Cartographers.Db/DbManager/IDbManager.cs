﻿using Cartographers.DTO;
using Cartographers.DTO.Db;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cartographers.DbApi.Data
{
    public interface IDbManager
    {
        public Task<AuthenticationDTO> RegisterAsync(Gamer gamer);
        public Task<AuthenticationDTO> LoginAsync(Gamer gamer);
        public Task SetGameScoreAsync(IEnumerable<GamerScore> scores);
    }
}
