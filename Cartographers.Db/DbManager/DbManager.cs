﻿using Cartographers.DTO;
using Cartographers.DTO.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cartographers.DbApi.Data
{
    public class DbManager : IDbManager
    {
        private readonly IDbContext _context;

        public DbManager(IDbContext context)
        {
            _context = context;
        }

        public async Task<AuthenticationDTO> RegisterAsync(Gamer gamer)
        {
            var exists = await _context.GetGamerAsync(gamer.Login) is not null;
            var message = exists ? $"Логин {gamer.Login} уже зарегистрирован" : $"Игрок {gamer.Login} успешно зарегистрирован";
            if (!exists) await _context.AddGamerAsync(gamer);
            return new AuthenticationDTO(!exists, message, gamer);
        }

        public async Task<AuthenticationDTO> LoginAsync(Gamer gamer)
        {
            var exists = await _context.GetGamerAsync(gamer.Login) is not null;
            var message = exists ? string.Empty : $"Игрок {gamer.Login} не найден или пароль введен неверно";
            return new AuthenticationDTO(exists, message, gamer);
        }

        public async Task SetGameScoreAsync(IEnumerable<GamerScore> scores)
        {
            var newGames = await GetNewGamesAsync(scores);
            if (newGames is not null)
            {
                await _context.AddGamesAsync(newGames);
            }
        }

        async Task<IEnumerable<Game>> GetNewGamesAsync(IEnumerable<GamerScore> scores)
        { 
            var games = await _context.GetGamesAsync(scores.Select(s => s.SessionUid).First());
            var newGames = scores.Where(w => !games.Any(a => a.Gamer.Login == w.GamerLogin));
            if (!newGames.Any()) return null;
            var sessionUid = newGames.First().SessionUid;
            var session = await _context.GetSessionAsync(newGames.First().SessionUid) ?? new Session() { Uid = sessionUid, Date = DateTime.Now };
            return newGames.Select(s => new Game()
                   {
                       Session = session,
                       Gamer = _context.Gamers.First(f => f.Login == s.GamerLogin),
                       Status = _context.Statuses.FirstOrDefault(f => f.Code == s.Status) ?? new Status() { Code = s.Status },
                       Score = s.CountScore(),
                       Date = DateTime.Now
                   });
        }
    }
}
