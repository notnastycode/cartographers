﻿using Cartographers.DTO.Db;
using Microsoft.EntityFrameworkCore;

namespace Cartographers.DbApi.Data
{
    static class DbModelCreating
    {
        internal static void EnsureEntitiesConfigured(this ModelBuilder builder)
        {
            builder.Entity<Gamer>().HasData
                (
                    new Gamer { Id = 1, FirstName = "Roman", LastName = "Kuznetsov", Email = "romankuz@gmail.com", Login = "Sonmor", Password = "123" },
                    new Gamer { Id = 2, FirstName = "Anton", LastName = "Khlevnoy", Email = "toksel@gmail.com", Login = "Q", Password = "123" },
                    new Gamer { Id = 3, FirstName = "F", Login = "F", Password = "12345" }
                );
            builder.Entity<Status>().HasData
                (
                    new Status { Id = 1, Code = "Finished" },
                    new Status { Id = 2, Code = "Canceled" }
                );
            builder.Entity<Session>().HasKey(k => k.Uid);
            builder.Entity<Gamer>()
                   .HasIndex(i => i.Login)
                   .HasDatabaseName("UC_Gamers_Login")
                   .IsUnique();
        }
    }
}
