using Cartographers.DbApi.Data;
using Cartographers.Rabbit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace Cartographers.DbApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Configuration.Bind("DbConfiguration", new Config());
            if (Config.UseSqlLite)
            {
                services.AddDbContext<AppDbContext>(x => x.UseSqlite(@"Data Source=Gamers.db"), ServiceLifetime.Singleton);
            }
            else
            {
                services.AddDbContext<AppDbContext>(x => x.UseNpgsql(Config.ConnectionString), ServiceLifetime.Singleton);
            }
            services.AddSingleton<IDbContext, AppDbContext>();
            services.AddSingleton<IDbManager, DbManager>();
            services.AddSingleton<IQueue, Queue>();
            services.AddSingleton(Settings.GetDefault());
            services.AddControllers();
            services.AddHostedService<AuthenticationManager>();
            services.AddHostedService<GameManager>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Cartographers.DbApi", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (true)
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Cartographers.DbApi v1"));
            }

            if (env.IsDevelopment())
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
        }
    }
}
