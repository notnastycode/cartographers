﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.Domain.ResearchCardData;
using Cartographers.Domain.SeasonData;
using Cartographers.Domain.Surfaces;
using Cartographers.DTO;
using System.Collections.Generic;
using System.Linq;

namespace Cartographers.Domain.Game
{
	public class Game
	{
		public Map Map { get; protected set; }
		public Figure Figure { get; protected set; }
		public Season Season { get; protected set; }
		public IDecree[] Decrees { get; protected set; }
		public int SeasonTurn { get; protected set; }
		public IResearchCard Card { get; protected set; }
		public int Score { get; protected set; }
		public int Coins { get; protected set; }
		public int Monsters { get; protected set; }
		public bool GameHasEnded => Season.Code == SeasonCode.Winter && SeasonTurn >= Season.Duration;
		public bool GameCancelled { get; set; }
		public bool TurnAvailable { get; set; }
		public int TurnTimeLeft { get; set; }

		protected int CurrentShapeIndex = 0;
		protected int CurrentFieldIndex = 0;
		protected List<Point> _surroundedMountains = new List<Point>();

		public Game(GameDTO gameDto, ResearchCardFactory rcf, DecreeFactory df)
		{
			Map = new Map(gameDto.Board);
			Card = rcf.GetResearchCardByCode((ResearchCode)gameDto.ResearchCardId);
			Card.IsRuin = gameDto.IsRuinCard;
			TakeFigureFromCard();
			SeasonTurn = gameDto.SeasonTurn;
			Season = SeasonProvider.GetSeasonByIndex(gameDto.SeasonId);
			Decrees = gameDto.DecreeIds.Select(id => df.GetByCode((DecreeCode)id)).ToArray();
			TurnAvailable = gameDto.TurnAvailable;
			Score = gameDto.Score;
			TurnTimeLeft = gameDto.TurnTimeLeft;
			GameCancelled = gameDto.GameCancelled;
			Coins = gameDto.Coins;
			Monsters = gameDto.Monsters;
		}

		public void SetShapeIndex(int index)
		{
			CurrentShapeIndex = index;
			TakeFigureFromCard();
		}

		public void SetFieldIndex(int index)
		{
			CurrentFieldIndex = index;
			TakeFigureFromCard(new Shape { Contour = Figure, HasGoldCoin = Card.Shapes[CurrentShapeIndex].HasGoldCoin });
		}

		protected void TakeFigureFromCard(Shape currentShape = null)
		{
			Figure = new Figure(currentShape ?? Card.Shapes[CurrentShapeIndex], Card.Fields[CurrentFieldIndex]);
		}

		public bool CanMove(Point position)
		{
			return Map.CanAdd(Figure, position, Card.IsRuin);
		}

		public virtual void Move(Point position)
		{
			Map.Move(Figure, position);
			AddShapeCoins();
			AddSurroundedMountains();
			CountMonsters();
			SeasonTurn += Card?.MovePoints ?? 1;
			if (GameHasEnded)
				return;
			if (Season.Duration <= SeasonTurn)
			{
				SeasonTurn = 0;
				Season = SeasonProvider.GetSeasonByIndex((int) Season.Code + 1);
			}
		}

		internal void AddShapeCoins()
		{
			if (Card.Shapes[CurrentShapeIndex].HasGoldCoin) { Coins++; }
		}

		internal void AddSurroundedMountains()
		{
			var newSurroundedMountains = Map.GetFieldsOnMapWithNeighbor()
											.Where(w => w.Field == FieldType.Mountain &&
																	w.Neighbors.All(a => a.Field != FieldType.Empty &&
																						a.Field != FieldType.None &&
																						a.Field != FieldType.Ruin))
											.Select(s => (Point)(s.CoordI, s.CoordJ))
											.Except(_surroundedMountains);
			if (newSurroundedMountains.Any())
			{
				Coins += newSurroundedMountains.Count();
				_surroundedMountains.AddRange(newSurroundedMountains);
			}
		}

		internal void CountMonsters()
		{
			Monsters = Map.GetFieldsOnMapWithNeighbor()
						.Where(w => w.Field == FieldType.Monster)
						.SelectMany(s => s.Neighbors)
						.Where(w => w.Field == FieldType.Empty || w.Field == FieldType.Ruin)
						.GetDistincts()
						.Count();
		}

		public void RotateFigure()
		{
			Figure.Rotate();
		}

		public void ReflectFigure(bool vertical = false)
		{
			if (vertical)
				Figure.ReflectVertical();
			else
				Figure.ReflectHorizontal();
		}

		protected IDecree[] GetPreviousSeasonDecrees()
		{
			int currentSeasonIndex = (int)Season.Code;
			return new[] { Decrees[(currentSeasonIndex + 3) % 4], Decrees[currentSeasonIndex] };
		}
	}
}
