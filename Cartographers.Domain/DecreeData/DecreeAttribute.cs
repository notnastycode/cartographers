﻿using System;

namespace Cartographers.Domain.DecreeData
{
    class DecreeAttribute : Attribute
    {
        public DecreeCode Code { get; set; }
    }
}
