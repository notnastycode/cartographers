﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Cartographers.Domain.DecreeData
{
    public class DecreeFactory : IEnumerable<IDecree>
    {
        private Dictionary<DecreeCode, Type> _decrees = new Dictionary<DecreeCode, Type>();

        public DecreeFactory()
        {
            Init();
        }

        private void Init()
        {
            var researchType = typeof(IDecree);
            foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (researchType.IsAssignableFrom(type) && !type.IsInterface && type.CustomAttributes.Any(a => a.AttributeType == typeof(DecreeAttribute)))
                {
                    _decrees[((DecreeAttribute)type.GetCustomAttribute(typeof(DecreeAttribute))).Code] = type;
                }
            }
        }

        public IDecree GetByCode(DecreeCode code)
            => (IDecree)Activator.CreateInstance(_decrees[code]);

        public IDecree GetById(int id) => GetByCode((DecreeCode)id);

        public IDecree[] GetDecrees()
            => _decrees.Select(s => s.Value)
                       .Shuffle()
                       .Take(4)
                       .Select(s => (IDecree)Activator.CreateInstance(s))
                       .ToArray();

        public IEnumerator<IDecree> GetEnumerator()
        {
            foreach (var type in _decrees.Select(s => s.Value))
            {
                yield return (IDecree)Activator.CreateInstance(type);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }
}
