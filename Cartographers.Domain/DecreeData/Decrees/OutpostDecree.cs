﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.Outpost)]
    public class OutpostDecree : Decree
    {
        public OutpostDecree() :
            base
            (
                code: DecreeCode.Outpost,
                name: "Форпост",
                description: "Получите две монеты за каждую клетку во второй по величине области из клеток поселений (она может быть равна по величине самой большой такой области)."
            ) { }

        public override int CountScore(Map map)
        {
            var towns = map.GetAreasOfType(FieldType.Town);
            return towns.Count > 1 ?
                towns.OrderByDescending(o => o.Fields.Count)
                     .Skip(1)
                     .First()
                     .Fields
                     .Count * 2 : 0;
        }
    }
}