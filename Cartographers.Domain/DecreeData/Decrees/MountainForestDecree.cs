﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Collections.Generic;
using System.Linq;
using static Cartographers.Domain.DecreeData.DecreeHelpers.Extensions;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.MountainForest)]
    public class MountainForestDecree : Decree
    {
        public MountainForestDecree() :
            base
            (
                code: DecreeCode.MountainForest,
                name: "Горнолесье",
                description: "Получите три монеты за каждую клетку гор, соединённую с другой клеткой гор областью из клеток леса."
            ) { }

        public override int CountScore(Map map)
        {
            var mountainFields = new List<FieldOnMap>();
            var forestAreas = map.GetAreasWithNeighborOfType(FieldType.Forest);
            forestAreas.ForEach(a =>
            {
                var neighborMountains = a.Neighbors.Where(n => n.Field == FieldType.Mountain);
                if (neighborMountains.Count() >= 2)
                {
                    mountainFields.AddRange(neighborMountains);
                }
            });
            mountainFields = mountainFields.GetDistincts().ToList();
            return mountainFields.Count * 3;
        }
    }
}
