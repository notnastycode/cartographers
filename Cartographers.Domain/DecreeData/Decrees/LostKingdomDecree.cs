﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.Domain.Surfaces;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.LostKingdom)]
    public class LostKingdomDecree : Decree
    {
        public LostKingdomDecree() :
            base
            (
                code: DecreeCode.LostKingdom,
                name: "Затерянное княжество",
                description: "Получите три монеты за каждую клетку вдоль одной стороны самого большого квадрата, полностью состоящего из заполненных клеток."
            ) { }

        public override int CountScore(Map map)
            => map.GetMaxFilledSquareSideSize() * 3;
    }
}