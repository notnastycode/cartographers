﻿using Cartographers.Domain.DecreeData.DecreeHelpers;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using System.Linq;

namespace Cartographers.Domain.DecreeData
{
    [Decree(Code = DecreeCode.WizardValley)]
    public class WizardValleyDecree : Decree
    {
        public WizardValleyDecree() :
            base
            (
                code: DecreeCode.WizardValley,
                name: "Долина волшебников",
                description: "Получите две монеты за каждую клетку водоёма, примыкающую к клетке гор. " +
                             "Получите одну монету за каждую клетку полей, примыкающую к клетке гор."
            ) { }

        public override int CountScore(Map map)
            => map.GetFieldsOnMapWithNeighbor()
                  .Count(c => c.Field.IsEaual(FieldType.Farm) && c.Neighbors.Any(n => n.Field == FieldType.Mountain)) +
               map.GetFieldsOnMapWithNeighbor()
                  .Count(c => c.Field.IsEaual(FieldType.River) && c.Neighbors.Any(n => n.Field == FieldType.Mountain)) * 2;
    }
}