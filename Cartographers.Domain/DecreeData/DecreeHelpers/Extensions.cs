﻿using Cartographers.Domain.Surfaces;
using Cartographers.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cartographers.Domain.DecreeData.DecreeHelpers
{
    public static class Extensions
    {
        public class FieldOnMap
        {
            public int CoordI { get; set; }
            public int CoordJ { get; set; }
            public FieldType Field { get; set; }
        }

        public class FieldOnMapWithNeighbor : FieldOnMap
        {
            public List<FieldOnMap> Neighbors { get; set; } // start from top and clockwise
        }

        public class AreaOnMap
        {
            public List<FieldOnMap> Fields { get; set; }
        }

        public class AreaOnMapWithNeighbor : AreaOnMap
        {
            public List<FieldOnMap> Neighbors { get; set; }
        }

        public static List<FieldOnMap> GetFieldsOnMap(this Map map)
        {
            var fieldsOnMap = new List<FieldOnMap>();
            for (int i = 0; i < map.Width; ++i)
                for (int j = 0; j < map.Height; ++j)
                    fieldsOnMap.Add(new FieldOnMap
                    {
                        CoordI = i,
                        CoordJ = j,
                        Field = map[i, j]
                    });
            return fieldsOnMap;
        }

        public static List<FieldOnMapWithNeighbor> GetFieldsOnMapWithNeighbor(this Map map)
        {
            var fieldsOnMap = new List<FieldOnMapWithNeighbor>();
            for (int i = 0; i < map.Width; ++i)
                for (int j = 0; j < map.Height; ++j)
                {
                    var neighbors = GetNeighbors(map, i, j);
                    fieldsOnMap.Add(new FieldOnMapWithNeighbor
                    {
                        CoordI = i,
                        CoordJ = j,
                        Field = map[i, j],
                        Neighbors = neighbors
                    });
                }
            return fieldsOnMap;
        }

        public static List<List<FieldType>> GetRows(this Map map)
        {
            var rows = new List<List<FieldType>>();
            for (int i = 0; i < map.Width; ++i)
            {
                var row = new List<FieldType>();
                for (int j = 0; j < map.Height; ++j)
                {

                    row.Add(map[i, j]);
                }
                rows.Add(row);
            }
            return rows;
        }

        public static List<List<FieldType>> GetColumns(this Map map)
        {
            var columns = new List<List<FieldType>>();
            for (int j = 0; j < map.Height; ++j)
            {
                var column = new List<FieldType>();
                for (int i = 0; i < map.Width; ++i)
                {

                    column.Add(map[i, j]);
                }
                columns.Add(column);
            }
            return columns;
        }

        public static IEnumerable<FieldOnMap> GetBorderFields(this Map map)
        {
            return map.GetFieldsOnMap().Where(f => f.IsBorderField(map));
        }

        public static bool IsBorderField(this FieldOnMap field, Map map)
        {
            return field.CoordI == 0 || field.CoordJ == 0 ||
                field.CoordI == map.Width - 1 || field.CoordJ == map.Height - 1;
        }

        public static List<AreaOnMap> GetAreasOfType(this Map map, FieldType type)
        {
            var areas = new List<AreaOnMap>();
            var fields = map.GetFieldsOnMap().Where(f => f.Field.IsEaual(type));
            while (fields?.Count() > 0)
            {
                var fieldsToProcess = new Stack<FieldOnMap>();
                var processedFields = new List<FieldOnMap>();
                fieldsToProcess.Push(fields.First());
                fields = fields.Skip(1);
                while (fieldsToProcess.Count > 0)
                {
                    var fieldToProcess = fieldsToProcess.Pop();
                    var groups = fields?.GroupBy(f => IsAdjacentFields(f, fieldToProcess)).ToDictionary(g => g.Key, g => g.Select(i => i));
                    groups?.TryGetValue(false, out fields);
                    if (groups != null && groups.TryGetValue(true, out var AdjacentFields))
                        foreach (var f in AdjacentFields)
                        {
                            fieldsToProcess.Push(f);
                        }
                    processedFields.Add(fieldToProcess);
                }
                if (processedFields.Count > 1)
                {
                    areas.Add(new AreaOnMap
                    {
                        Fields = processedFields
                    });
                }
            }
            return areas;
        }

        public static List<AreaOnMapWithNeighbor> GetAreasWithNeighborOfType(this Map map, FieldType type)
        {
            var areas = map.GetAreasOfType(type);
            return areas.Select(a => TransformToAreaWithNeighbor(map, a)).ToList();
        }

        public static bool IsTerritoryField(this FieldType field)
        {
            return (field != FieldType.Empty) && (field != FieldType.Ruin);
        }

        public static int GetMaxFilledSquareSideSize(this Map map)
        {
            int nextMaxSize = 1;
            for (int i = 0; i < map.Width; ++i)
                for (int j = 0; j < map.Height; ++j)
                    while (HasFilledSquareAt(map, i, j, nextMaxSize))
                        ++nextMaxSize;
            return nextMaxSize - 1;
        }

        private static bool HasFilledSquareAt(Map map, int coordI, int coordJ, int size)
        {
            if (coordI + size - 1 >= map.Width)
                return false;
            if (coordJ + size - 1 >= map.Height)
                return false;
            for (int i = coordI; i < coordI + size; ++i)
                for (int j = coordJ; j < coordJ + size; ++j)
                    if (map[i, j] == FieldType.Empty 
                        || map[i, j] == FieldType.None 
                        || map[i, j] == FieldType.Ruin )
                        return false;
            return true;
        }

        public static int GetFilledLeftBottomDiagonalsCount(this Map map)
        {
            int count = 0;
            for (int i = 0; i < map.Width; ++i)
                if (IsFilledLeftBottomDiagonal(map, i))
                    ++count;
            return count;
        }

        public static IEnumerable<FieldOnMap> GetDistincts(this IEnumerable<FieldOnMap> fields)
        {
            return fields.Distinct(new FieldComparer());
        }

        private static bool IsFilledLeftBottomDiagonal(Map map, int leftSizeCoordI)
        {
            for (int i = 0; i < leftSizeCoordI + 1; ++i)
                if (map[leftSizeCoordI - i, i] == FieldType.Empty || map[leftSizeCoordI - i, i] == FieldType.Ruin)
                    return false;
            return true;
        }

        private static bool IsAdjacentFields(FieldOnMap field1, FieldOnMap field2)
        {
            return (Math.Abs(field1.CoordI - field2.CoordI) + Math.Abs(field2.CoordJ - field1.CoordJ)) == 1;
        }

        private static AreaOnMapWithNeighbor TransformToAreaWithNeighbor(Map map, AreaOnMap area)
        {
            IEnumerable<FieldOnMap> neighbors = new List<FieldOnMap>();
            area.Fields.ForEach(f =>
            {
                neighbors = neighbors.Concat(GetNeighbors(map, f).Where(nf => nf.Field != f.Field));
            });
            neighbors = neighbors.GetDistincts();
            return new AreaOnMapWithNeighbor
            {
                Fields = area.Fields,
                Neighbors = neighbors.ToList()
            };
        }

        public static List<FieldOnMap> GetNeighbors(Map map, FieldOnMap field)
        {
            return GetNeighbors(map, field.CoordI, field.CoordJ);
        }

        private static List<FieldOnMap> GetNeighbors(Map map, int coordI, int coordJ)
        {
            var neighbors = new List<FieldOnMap>();
            if (coordI > 0)
                neighbors.Add(new FieldOnMap
                {
                    Field = map[coordI - 1, coordJ],
                    CoordI = coordI - 1,
                    CoordJ = coordJ
                });
            if (coordJ < map.Height - 1)
                neighbors.Add(new FieldOnMap
                {
                    Field = map[coordI, coordJ + 1],
                    CoordI = coordI,
                    CoordJ = coordJ + 1
                });
            if (coordI < map.Width - 1)
                neighbors.Add(new FieldOnMap
                {
                    Field = map[coordI + 1, coordJ],
                    CoordI = coordI + 1,
                    CoordJ = coordJ
                });
            if (coordJ > 0)
                neighbors.Add(new FieldOnMap
                {
                    Field = map[coordI, coordJ - 1],
                    CoordI = coordI,
                    CoordJ = coordJ - 1
                });
            return neighbors;
        }

        private class FieldComparer : IEqualityComparer<FieldOnMap>
        {
            public bool Equals(FieldOnMap x, FieldOnMap y)
            {
                if (Object.ReferenceEquals(x, y)) return true;
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;
                return x.CoordI == y.CoordI && x.CoordJ == y.CoordJ;
            }

            public int GetHashCode(FieldOnMap field)
            {
                if (Object.ReferenceEquals(field, null)) return 0;
                return field.CoordI * 100 + field.CoordJ;
            }
        }
    }
}
