﻿using Cartographers.DTO;
using Cartographers.DTO;
using Cartographers.Domain.ResearchCardData;
using System.Text;

namespace Cartographers.Domain.Surfaces
{
	public class Board
	{
		public int Height { get; protected set; }
		public int Width { get; protected set; }
		protected FieldType[,] _fields;

		public Board(BoardDTO board)
		{
			Height = board.Height;
			Width = board.Width;
			_fields = new FieldType[Height, Width];
			for (int i = 0; i < Width; ++i)
				for (int j = 0; j < Height; ++j)
				{
					_fields[i, j] = board.Fields[i * Height + j];
				}
		}

		public Board(FieldType[,] fields)
		{
			Width = fields.GetLength(0);
			Height = fields.GetLength(1);
			_fields = fields;
		}

		public Board(Shape shape, FieldType type)
		{
			Width = shape.Contour.GetLength(0);
			Height = shape.Contour.GetLength(1);
			_fields = new FieldType[Width, Height];
			for (int i = 0; i < Width; ++i)
				for (int j = 0; j < Height; ++j)
					_fields[i, j] = shape.Contour[i, j] ? type : FieldType.None;
		}

		public FieldType this[int i, int j]
		{
			get
			{
				return _fields[i, j];
			}
			set
			{
				_fields[i, j] = value;
			}
		}

		public BoardDTO GetDTO()
		{
			var fields = new FieldType[Height * Width];
			for (int i = 0; i < Width; ++i)
				for (int j = 0; j < Height; ++j)
				{
					fields[i * Height + j] = _fields[i, j];
				}
			return new BoardDTO { Fields = fields, Height = Height, Width = Width };
		}

		public FieldType[,] GetFields() => _fields;

		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.Append($"(Height:{Width});");
			sb.Append($"(Width:{Height});");
			for (int x = 0; x < _fields.GetLength(0); x++)
			{
				for (int y = 0; y < _fields.GetLength(1); y++)
				{
					if (_fields[x, y] != FieldType.Empty)
					{
						sb.Append($"({x},{y},{_fields[x, y]}),");
					}
				}
			}
			return sb.Length > 1 ? sb.ToString().Substring(0, sb.Length - 1) : string.Empty;
		}
    }
}
