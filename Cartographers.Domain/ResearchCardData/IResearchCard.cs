﻿using Cartographers.DTO;
using System;

namespace Cartographers.Domain.ResearchCardData
{
    public interface IResearchCard : IEquatable<IResearchCard>
	{
		ResearchCode Code { get; }
		FieldType[] Fields { get; }
		Shape[] Shapes { get; }
		string Name { get; }
		int MovePoints { get; }
		bool IsMonster { get; }
		bool IsRuin { get; set; }
	}
}
