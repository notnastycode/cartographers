﻿using System.Text;

namespace Cartographers.Domain.ResearchCardData
{
	public class Shape
	{
		public bool[,] Contour { get; set; }
		public bool HasGoldCoin { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int x = 0; x < Contour.GetUpperBound(0) + 1; x++)
            {
                for (int y = 0; y < Contour.GetUpperBound(1) + 1; y++)
                {
                    if (Contour[x, y])
                    {
                        sb.Append("" + x + y + "_");
                    }
                }
            }
            return sb.ToString().Substring(0, sb.Length - 1);
        }
    }
}
