﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.GreatRiver)]
   class GreatRiverResearchCard : ResearchCard
   {
      public GreatRiverResearchCard()
      {
         Code = ResearchCode.GreatRiver;
         Name = "Великая река";
         MovePoints = 1;
         Fields = new FieldType[]
         {
            FieldType.River
         };
         bool[,] shape311 = new bool[1, 3];
         shape311[0, 0] = true;
         shape311[0, 1] = true;
         shape311[0, 2] = true;
         bool[,] shape520 = new bool[3, 3];
         shape520[0, 0] = true;
         shape520[1, 0] = true;
         shape520[1, 1] = true;
         shape520[2, 1] = true;
         shape520[2, 2] = true;
         Shapes = new Shape[]
         {
            new Shape(){ Contour = shape311, HasGoldCoin = true },
            new Shape(){ Contour = shape520 }
         };
      }
   }
}
