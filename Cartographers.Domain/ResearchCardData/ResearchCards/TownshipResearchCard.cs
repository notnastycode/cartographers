﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.Township)]
   class TownshipResearchCard : ResearchCard
   {
      public TownshipResearchCard()
      {
         Code = ResearchCode.Township;
         Name = "Городок";
         MovePoints = 1;
         Fields = new FieldType[]
         {
            FieldType.Town
         };
         bool[,] shape301 = new bool[2, 2];
         shape301[0, 0] = true;
         shape301[0, 1] = true;
         shape301[1, 0] = true;
		 bool[,] shape530 = new bool[3, 2];
         shape530[0, 1] = true;
         shape530[1, 1] = true;
         shape530[2, 1] = true;
         shape530[0, 0] = true;
         shape530[1, 0] = true;
         Shapes = new Shape[]
         {
            new Shape(){ Contour = shape301, HasGoldCoin = true },
            new Shape(){ Contour = shape530 }
         };
      }
   }
}
