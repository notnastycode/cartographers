﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.GoblinAttack, IsMonster = true)]
   class GoblinAttackMonsterCard : ResearchCard
   {
      public GoblinAttackMonsterCard()
      {
         Code = ResearchCode.GoblinAttack;
         Name = "Атака гоблинов";
         MovePoints = 0;
         Fields = new FieldType[]
         {
            FieldType.Monster
         };
         bool[,] shape310 = new bool[3, 3];
         shape310[0, 2] = true;
         shape310[1, 1] = true;
         shape310[2, 0] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape310 }
         };
      }
   }
}
