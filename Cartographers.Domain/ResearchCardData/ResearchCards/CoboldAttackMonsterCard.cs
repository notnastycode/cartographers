﻿using Cartographers.DTO;

namespace Cartographers.Domain.ResearchCardData
{
    [ResearchCard(ResearchCode = ResearchCode.CoboldAttack, IsMonster = true)]
   class CoboldAttackMonsterCard : ResearchCard
   {
      public CoboldAttackMonsterCard()
      {
         Code = ResearchCode.CoboldAttack;
         Name = "Рейд кобольдов";
         MovePoints = 0;
         Fields = new FieldType[]
         {
            FieldType.Monster
         };
         bool[,] shape410 = new bool[2, 3];
         shape410[0, 2] = true;
         shape410[0, 1] = true;
         shape410[0, 0] = true;
         shape410[1, 1] = true;
         Shapes = new Shape[]
         {
            new Shape() { Contour = shape410 }
         };
      }
   }
}