﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Cartographers.Domain.ResearchCardData
{
   public class ResearchCardFactory : IEnumerable<IResearchCard>
   {
		private Dictionary<ResearchCode, Type> _researchCards = new Dictionary<ResearchCode, Type>();
		private Dictionary<ResearchCode, Type> _monsterCards = new Dictionary<ResearchCode, Type>();

		public ResearchCardFactory()
		{
			Init();
		}

		private void Init()
		{
			var researchType = typeof(IResearchCard);
			foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
			{
				if (researchType.IsAssignableFrom(type) && !type.IsInterface && type.CustomAttributes.Any())
				{
					AddType(
						((ResearchCardAttribute)type.GetCustomAttribute(typeof(ResearchCardAttribute))).IsMonster,
						((ResearchCardAttribute)type.GetCustomAttribute(typeof(ResearchCardAttribute))).ResearchCode,
						type);
				}
			}
		}

		private void AddType(bool isMonster, ResearchCode code, Type t)
		{
			if (isMonster)
			{
				_monsterCards.Add(code, t);
			}
			else
			{
				_researchCards.Add(code, t);
			}
		}

		public IResearchCard GetResearchCardByCode(ResearchCode code)
			=> (IResearchCard)Activator.CreateInstance(_researchCards.Concat(_monsterCards).First(f => f.Key == code).Value);

		public IEnumerable<IResearchCard> GetAllResearchCards()
			=> _researchCards.Select(s => (IResearchCard)Activator.CreateInstance(s.Value));

		public IEnumerable<IResearchCard> GetAllMonsterCards()
			=> _monsterCards.Select(s => (IResearchCard)Activator.CreateInstance(s.Value));

		public IEnumerator<IResearchCard> GetEnumerator()
		{
			foreach (var item in _researchCards.Concat(_monsterCards).Select(s => s.Value))
			{
				yield return (IResearchCard)Activator.CreateInstance(item);
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
			=> GetEnumerator();
   }
}
