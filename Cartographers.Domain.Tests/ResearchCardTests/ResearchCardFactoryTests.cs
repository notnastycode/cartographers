﻿using Cartographers.Domain.DecreeData;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using NUnit.Framework;

namespace Cartographers.Domain.Tests.ResearchTests

{
    [TestFixture]
    public class ResearchCardFactoryTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void CountScores_NoForest_ReturnsZeroScore()
        {
            int expectedScore = 0;
            FieldType[,] fieldTypes = new FieldType[2, 2] 
            { { FieldType.Empty, FieldType.Empty }, { FieldType.Empty, FieldType.Empty } };
            Map map = new Map(fieldTypes);

            var actualScore = _df.GetByCode(DecreeCode.GreenRegion).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }
    }
}
