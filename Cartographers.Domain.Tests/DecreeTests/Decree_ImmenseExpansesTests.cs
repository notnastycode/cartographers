﻿using NUnit.Framework;
using Cartographers.Domain.Surfaces;
using Cartographers.Domain.DecreeData;
using Cartographers.DTO;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_ImmenseExpansesTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void CountScores_EmtyFields_ReturnsZeroScore()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.ImmenseExpanses).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_NotFullLine_ReturnsZeroScore()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[1, 1] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.ImmenseExpanses).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_1FUllLine_Returns4Scores()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Town;
            fields[0, 1] = FieldType.RuinFarm;
            fields[0, 2] = FieldType.River;
            fields[0, 3] = FieldType.Town;
            fields[3, 3] = FieldType.Farm;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.ImmenseExpanses).CountScore(map);
            Assert.AreEqual(6, actualScore);
        }

        [Test]
        public void CountScores_1AnotherLine_Returns6Scores()
        {
            int expectedScore = 6;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Town;
            fields[1, 0] = FieldType.Farm;
            fields[2, 0] = FieldType.Town;
            fields[3, 0] = FieldType.Town;           
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.ImmenseExpanses).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_Mix2Lines_Returns4Scores()
        {
            int expectedScore = 12;
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[2, 0] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Town;
            fields[2, 3] = FieldType.Town;
            fields[2, 4] = FieldType.River;
            fields[0, 2] = FieldType.Town;
            fields[1, 2] = FieldType.Forest;
            fields[3, 2] = FieldType.Town;
            fields[4, 2] = FieldType.Town;

            Map map = new Map(fields);

            var actualScore = _df.GetByCode(DecreeCode.ImmenseExpanses).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_Mix2LinesMountains_0Scores()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[2, 0] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Ruin;
            fields[2, 3] = FieldType.Town;
            fields[2, 4] = FieldType.Mountain;
            fields[0, 2] = FieldType.Town;
            fields[1, 2] = FieldType.Forest;
            fields[3, 2] = FieldType.Town;
            fields[4, 2] = FieldType.Town;

            Map map = new Map(fields);

            var actualScore = _df.GetByCode(DecreeCode.ImmenseExpanses).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

    }
}

