﻿using Cartographers.Domain.DecreeData;
using Cartographers.DTO;
using Cartographers.Domain.Surfaces;
using NUnit.Framework;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_WatchtoerForestTests
    {
        DecreeFactory _df;

        [SetUp]
        public void Setup()
        {
            _df = new DecreeFactory();
        }

        [Test]
        public void CountScores_EmptyFields_ReturnsZeroScore()
        {
            int expectedScore = 0;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.WatchtowerForest).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_1SCore_ReturnsZeroScore()
        {
            int expectedScore = 1;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.Forest;
            fields[2, 1] = FieldType.Forest;
            fields[2, 2] = FieldType.Forest;
            fields[2, 3] = FieldType.Town;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.WatchtowerForest).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_5Forests_Returns5Scores()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Forest;
            fields[0, 1] = FieldType.Forest;
            fields[0, 2] = FieldType.Forest;
            fields[0, 3] = FieldType.Forest;
            fields[3, 3] = FieldType.Forest;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.WatchtowerForest).CountScore(map);
            Assert.AreEqual(5, actualScore);
        }

        [Test]
        public void CountScores_1AnotherLine_Returns6Scores()
        {
            int expectedScore = 4;
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Forest;
            fields[1, 0] = FieldType.Forest;
            fields[2, 0] = FieldType.Forest;
            fields[3, 0] = FieldType.Forest;
            Map map = new Map(fields);
            var actualScore = _df.GetByCode(DecreeCode.WatchtowerForest).CountScore(map);
            Assert.AreEqual(expectedScore, actualScore);
        }

        [Test]
        public void CountScores_Mix2Lines_Returns4Scores()
        {
            int expectedScore = 4;
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[2, 0] = FieldType.Forest;
            fields[2, 1] = FieldType.Forest;
            fields[2, 2] = FieldType.Forest;
            fields[2, 3] = FieldType.Forest;
            fields[2, 4] = FieldType.Forest;
            fields[0, 2] = FieldType.Forest;
            fields[1, 2] = FieldType.Forest;
            fields[3, 2] = FieldType.Forest;
            fields[4, 2] = FieldType.Forest;

            Map map = new Map(fields);

            var actualScore = _df.GetByCode(DecreeCode.WatchtowerForest).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }

        public void CountScores_Mix2LinesMountains_Returns4Scores()
        {
            int expectedScore = 6;
            var fields = Extensions.GenerateEmptyFields(5, 5);
            fields[2, 0] = FieldType.Town;
            fields[2, 1] = FieldType.Town;
            fields[2, 2] = FieldType.Ruin;
            fields[2, 3] = FieldType.Town;
            fields[2, 4] = FieldType.Mountain;
            fields[0, 2] = FieldType.Town;
            fields[1, 2] = FieldType.Forest;
            fields[3, 2] = FieldType.Town;
            fields[4, 2] = FieldType.Town;

            Map map = new Map(fields);

            var actualScore = _df.GetByCode(DecreeCode.WatchtowerForest).CountScore(map);

            Assert.AreEqual(expectedScore, actualScore);
        }
    }
}
