﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.Surfaces;
using Cartographers.DTO;
using NUnit.Framework;


namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_PromisedLand
    {
        IDecree _promisedLand;

        [SetUp]
        public void Setup()
        {
            _promisedLand = new DecreeFactory().GetByCode(DecreeCode.PromisedLand);
        }

        [Test]
        public void CountScore_HasTwoNeighbours_Counts0()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.River;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[2, 2] = FieldType.Farm;

            Map map = new Map(fields);
            var actualScore = _promisedLand.CountScore(map);
            Assert.AreEqual(0, actualScore);
        }

        [Test]
        public void CountScore_HasThreeNeighbours_Counts3()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.River;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[1, 0] = FieldType.Forest;
            fields[2, 2] = FieldType.Farm;

            Map map = new Map(fields);
            var actualScore = _promisedLand.CountScore(map);
            Assert.AreEqual(3, actualScore);
        }

        [Test]
        public void CountScore_HasFilledRuin_Counts3()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.River;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[1, 0] = FieldType.RuinForest;
            fields[2, 2] = FieldType.Farm;

            Map map = new Map(fields);
            var actualScore = _promisedLand.CountScore(map);
            Assert.AreEqual(3, actualScore);
        }

        [Test]
        public void CountScore_HasMonster_Counts3()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.River;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[1, 0] = FieldType.Monster;
            fields[2, 2] = FieldType.Farm;

            Map map = new Map(fields);
            var actualScore = _promisedLand.CountScore(map);
            Assert.AreEqual(3, actualScore);
        }

        [Test]
        public void CountScore_HasThreeNeighboursOfTheSameType_Counts0()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 1] = FieldType.River;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Town;
            fields[1, 0] = FieldType.River;
            fields[2, 2] = FieldType.River;

            Map map = new Map(fields);
            var actualScore = _promisedLand.CountScore(map);
            Assert.AreEqual(0, actualScore);
        }
    }
}
