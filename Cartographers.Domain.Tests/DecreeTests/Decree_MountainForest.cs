﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.Surfaces;
using Cartographers.DTO;
using NUnit.Framework;

namespace Cartographers.Domain.Tests.DecreeTests
{
    class Decree_MountainForest
    {
        IDecree _mountainForest;

        [SetUp]
        public void Setup()
        {
            _mountainForest = new DecreeFactory().GetByCode(DecreeCode.MountainForest);
        }

        [Test]
        public void CountScore_ChainsTwoMountains_Counts6()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Mountain;
            fields[0, 1] = FieldType.Forest;
            fields[1, 1] = FieldType.Forest;
            fields[1, 2] = FieldType.Forest;
            fields[2, 2] = FieldType.Mountain;

            Map map = new Map(fields);
            var actualScore = _mountainForest.CountScore(map);
            Assert.AreEqual(6, actualScore);
        }

        [Test]
        public void CountScore_ChainsThreeMountains_Counts9()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Mountain;
            fields[1, 0] = FieldType.Forest;
            fields[2, 0] = FieldType.Mountain;
            fields[1, 1] = FieldType.Forest;
            fields[1, 2] = FieldType.Forest;
            fields[2, 2] = FieldType.Mountain;

            Map map = new Map(fields);
            var actualScore = _mountainForest.CountScore(map);
            Assert.AreEqual(9, actualScore);
        }

        [Test]
        public void CountScore_ChainsTwoMountainsWithRuinForest_Counts6()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Mountain;
            fields[0, 1] = FieldType.Forest;
            fields[1, 1] = FieldType.RuinForest;
            fields[1, 2] = FieldType.Forest;
            fields[2, 2] = FieldType.Mountain;

            Map map = new Map(fields);
            var actualScore = _mountainForest.CountScore(map);
            Assert.AreEqual(6, actualScore);
        }

        [Test]
        public void CountScore_HasFieldOfDifferentTypeInChain_Counts0()
        {
            var fields = Extensions.GenerateEmptyFields(4, 4);
            fields[0, 0] = FieldType.Mountain;
            fields[0, 1] = FieldType.Forest;
            fields[1, 1] = FieldType.Town;
            fields[1, 2] = FieldType.Forest;
            fields[2, 2] = FieldType.Mountain;

            Map map = new Map(fields);
            var actualScore = _mountainForest.CountScore(map);
            Assert.AreEqual(0, actualScore);
        }

    }
}
