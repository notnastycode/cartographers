﻿using Cartographers.Domain.DecreeData;
using Cartographers.DTO;
using Cartographers.Domain.SeasonData;
using Cartographers.Domain.Surfaces;
using Cartographers.DomainUI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class Seasons
    {
        IClientGameManager _gameManager;
        Season[] _seasonList = {
                                   new Season(SeasonCode.Spring, "Весна", 8),
                                   new Season(SeasonCode.Summer, "Лето" , 8),
                                   new Season(SeasonCode.Fall,   "Осень", 7),
                                   new Season(SeasonCode.Winter, "Зима" , 6),
                               };
        Dictionary<Season, IDecree[]> _seasons;
        Dictionary<int, int[]> _scoreByDecree;
        bool _clicked = false;
        Map _map;
        bool _newSeasonStartsNextTurn;
        int _score;

        public Seasons(IClientGameManager gameManager)
        {
            _gameManager = gameManager;
            UpdateSeasons();
            _scoreByDecree = _seasons.Select(s => new { Key = (int)s.Key.Code, Value = new int[2] { 0, 0 } }).ToDictionary(d => d.Key, d => d.Value);
        }

        public void Init()
            => Update();

        public void Update()
        {
            Clear();
            ShowSeasonsBackground();
            ShowHeaders();
            ShowDecreeIcons();
            ShowCoins();
            ShowMonsters();
            ShowScore();
        }

        public void Update(IClientGameManager gameManager, FieldType[,] map)
        {
            _gameManager = gameManager;
            _map = map;
            _score = gameManager.Score;
            UpdateSeasons();
            CalculateDecreeScore(gameManager);
        }

        public void Update(List<GamerScore> score)
        {
            _score = score.First(f => f.GamerLogin == _gameManager.GamerLogin).CountScore();
        }

        void UpdateSeasons()
        {
            _seasons = new Dictionary<Season , IDecree[]>
            {
                { _seasonList[0], new IDecree[2]{ _gameManager.Decrees[0], _gameManager.Decrees[1] } },
                { _seasonList[1], new IDecree[2]{ _gameManager.Decrees[1], _gameManager.Decrees[2] } },
                { _seasonList[2], new IDecree[2]{ _gameManager.Decrees[2], _gameManager.Decrees[3] } },
                { _seasonList[3], new IDecree[2]{ _gameManager.Decrees[3], _gameManager.Decrees[0] } },
            };
        }

        void CalculateDecreeScore(IClientGameManager gameManager)
        {
            var previousSeasionIndex = (int)gameManager.Season.Code - 1;
            if (_newSeasonStartsNextTurn && gameManager.SeasonTurn == 0)
            {
                var previousSeasion = _seasonList[previousSeasionIndex];
                _scoreByDecree[(int)gameManager.Season.Code - 1] = new int[2] { _seasons[previousSeasion][0].CountScore(_map), _seasons[previousSeasion][1].CountScore(_map) };
                _newSeasonStartsNextTurn = false;
            }
            _scoreByDecree[(int)gameManager.Season.Code] = new int[2] { _seasons[gameManager.Season][0].CountScore(_map), _seasons[gameManager.Season][1].CountScore(_map) };
            if (gameManager.SeasonTurn + gameManager.ResearchCard.MovePoints >= gameManager.Season.Duration)
            {
                _newSeasonStartsNextTurn = true;
            }
        }

        void ShowSeasonsBackground()
        {
            string seasonCode = _gameManager.Season.Code.ToString();
            GameObject seasonBackground = GameObject.Find(seasonCode);
            Vector2 gameWindowPosition = new Vector2(7, 5);
            if (seasonBackground.transform.position != (Vector3)gameWindowPosition)
            {
                Utils.DeleteGameObject(GameObject.FindGameObjectWithTag("SeasonBackground"));
                seasonBackground = GameObject.Instantiate(GameObject.Find(seasonCode));
                seasonBackground.transform.position = gameWindowPosition;
                seasonBackground.name = seasonCode;
                seasonBackground.tag = "SeasonBackground";
            }
        }

        void ShowHeaders()
        {
            for (int i = 0; i < _seasons.Count; i++)
            {
                bool currentSeason = _seasons.ElementAt(i).Key.Code == _gameManager.Season.Code;
                string turn = currentSeason ?
                    $": {_gameManager.SeasonTurn}/{_gameManager.Season.Duration}" :
                    string.Empty;
                string seasonLabel = $"{_seasons.ElementAt(i).Key.Name} {turn}";
                TextObject seasonHeader = new TextObject($"SeasonHeader_{_seasons.ElementAt(i).Key.Code}", seasonLabel);
                seasonHeader.SetPosition(new Vector2(-3, 11 - 3.25f * i));
                seasonHeader.SetFrame(new Vector2(8, 2));
                seasonHeader.SetScale(new Vector2(0.7f, 0.7f));
                seasonHeader.SetAlignment(TextAnchor.MiddleCenter);
                seasonHeader.SetFont("Header");
                seasonHeader.SetTag("SeasonObjects");
                seasonHeader.SetColor(currentSeason ? Color.black : Color.white);
                if (currentSeason)
                {
                   DrawSeasonBackground(new Vector2(-3, 10.1f - 3.25f * i));
                }
            }
        }

        void DrawSeasonBackground(Vector2 position)
        {
            UIObject background = new UIObject();
            background.Init("SeasonBackground", "CurrentSeasonBackground");
            background.SetPosition(position);
            background.SetScale(new Vector2(0.4f, 0.45f));
            background.SetTag("SeasonObjects");
            background.SetLayer(0);
        }

        void ShowDecreeIcons()
        {
            for (int i = 0; i < _seasons.Count; i++)
            {
                if (_seasons.ElementAt(i).Value[0].Code != DecreeCode.None)
                {
                    DrawDecreeIcon(_seasons.ElementAt(i).Value[0], i, 0, new Vector2(-4, 9.7f - 3.25f * i));
                }
                if (_seasons.ElementAt(i).Value[1].Code != DecreeCode.None)
                {
                    DrawDecreeIcon(_seasons.ElementAt(i).Value[1], i, 1, new Vector2(-2, 9.7f - 3.25f * i));
                }
            }
        }

        void DrawDecreeIcon(IDecree decree, int seasonNumber, int decreeIndex, Vector2 position)
        {
            ButtonObject bo = new ButtonObject
                (
                    objectName: decree.Code.ToString(),
                    action: (int index) => { ShowFullDecree(decree, seasonNumber, decreeIndex); },
                    newName: $"decree_icon_{decree.Code}"
                );
            bo.SetPosition(position);
            bo.SetLayer(1);
            bo.SetTag("SeasonObjects");
            bo.SetScale(new Vector2(0.2f, 0.2f));
        }

        void ShowFullDecree(IDecree decree, int seasonNumber, int decreeIndex)
        {
            UIObject icon = new UIObject();
            icon.Init(decree.Code.ToString(), "DecreeIconLagre");
            icon.SetScale(new Vector2(0.5f, 0.5f));
            icon.SetPosition(new Vector2(7, 7));
            icon.SetLayer(3);

            UIObject decreeBackground = new UIObject();
            decreeBackground.Init("ShapeBackground", "DecreeBackground");
            decreeBackground.SetScale(new Vector2(0.75f, 0.75f));
            decreeBackground.SetPosition(new Vector2(7.5f, 5));
            decreeBackground.SetLayer(2);

            TextObject decreeHeader = new TextObject("DecreeHeader", decree.Name);
            decreeHeader.SetPosition(new Vector2(7, 11));
            decreeHeader.SetFrame(new Vector2(8, 2));
            decreeHeader.SetScale(new Vector2(0.75f, 0.75f));
            decreeHeader.SetAlignment(TextAnchor.MiddleCenter);
            decreeHeader.SetLayer(3);
            decreeHeader.SetColor(Color.black);

            TextObject decreeDesctiption = new TextObject("DecreeDescription", decree.Description);
            decreeDesctiption.SetPosition(new Vector2(6.75f, 2));
            decreeDesctiption.SetFrame(new Vector2(13, 9));
            decreeDesctiption.SetScale(new Vector2(0.5f, 0.5f));
            decreeDesctiption.SetAlignment(TextAnchor.UpperLeft);
            decreeDesctiption.SetLayer(3);
            decreeDesctiption.SetColor(Color.black);

            string decreeScoreDescription = seasonNumber <= (int)_gameManager.Season.Code ?
                $"Счет за указ: {_scoreByDecree[seasonNumber][decreeIndex]}" :
                string.Empty;
            TextObject decreeScore = new TextObject("DecreeScore", decreeScoreDescription);
            decreeScore.SetPosition(new Vector2(6.75f, -1));
            decreeScore.SetFrame(new Vector2(13, 1.5f));
            decreeScore.SetScale(new Vector2(0.5f, 0.5f));
            decreeScore.SetAlignment(TextAnchor.MiddleLeft);
            decreeScore.SetLayer(3);
            decreeScore.SetColor(Color.black);
        }

        public void ButtonInteract()
        {
            if (Input.GetMouseButton(0) && !_clicked)
            {
                _clicked = true;
                DeleteFullDecree();

                Vector2 clickPosition = Utils.GetClickPosition();
                Transform clickedItem = Utils.GetItemAt(clickPosition);
                if (clickedItem != null && clickedItem.gameObject.tag == "SeasonObjects")
                {
                    clickedItem.gameObject.GetComponent<Button>().onClick.Invoke();
                }
            }
            if (!Input.GetMouseButton(0))
            {
                _clicked = false;
            }
        }

        void ShowScore()
        {
            string score = $"Счет: {_score}";
            TextObject scoreHeader = new TextObject("ScoreHeader", score);
            scoreHeader.SetPosition(new Vector2(17, 11.5f));
            scoreHeader.SetFrame(new Vector2(10, 2));
            scoreHeader.SetScale(new Vector2(0.7f, 0.7f));
            scoreHeader.SetAlignment(TextAnchor.MiddleCenter);
            scoreHeader.SetFont("Header");
            scoreHeader.SetTag("SeasonObjects");
            scoreHeader.SetColor(Color.white);
        }

        void ShowCoins()
        {
            for (int i = 0; i < _gameManager.Coins; i++)
            {
                UIObject coin = new UIObject();
                coin.Init("Coin", $"Coin#{i}");
                coin.SetScale(new Vector2(0.35f, 0.35f));
                coin.SetTag("SeasonObjects");
                coin.SetPosition(new Vector2(15 - 0.75f * i, 11.5f));
            }
        }

        void ShowMonsters()
        {
            for (int i = 0; i < _gameManager.Monsters; i++)
            {
                UIObject monster = new UIObject();
                monster.Init("MonsterIcon", $"Monster#{i}");
                monster.SetScale(new Vector2(0.35f, 0.35f));
                monster.SetTag("SeasonObjects");
                monster.SetPosition(new Vector2(15 - 0.75f * i, 10.75f));
            }
        }

        void DeleteFullDecree()
        {
            Utils.DeleteGameObject("DecreeIconLagre");
            Utils.DeleteGameObject("DecreeBackground");
            Utils.DeleteGameObject("DecreeHeader");
            Utils.DeleteGameObject("DecreeDescription");
            Utils.DeleteGameObject("DecreeScore");
        }

        public void Clear()
        {
            Utils.DeleteGameObjctsByTag("SeasonObjects");
        }
   }
}
