﻿using Cartographers.Domain.DecreeData;
using Cartographers.DTO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTable : MonoBehaviour
{
    private Transform _entryContainer;
    private Transform _entryTemplate;
    private const float _templateHight = 50f;
    private Vector2 _decreeIconScale = new Vector2(1f, 1f);
    private List<Transform> _scores;

    public List<GamerScore> Score { get; set; }
    public IDecree[] Decrees { get; set; }

    private void Awake()
    {
        _entryContainer = transform.Find("EntryContainer");
        _entryTemplate = _entryContainer.Find("EntryTemplate");
        _entryTemplate.gameObject.SetActive(false);
        gameObject.SetActive(false);
        _scores = new List<Transform>();
    }

    public void Show()
    {
        SetDecreeIcons();
        var score = Score.OrderByDescending(o => o.Coins.Sum() + o.DecreeScore.Sum() - o.Monsters.Sum()).ToList();
        for (int i = 0; i < score.Count; i++)
        {
            Transform entryTransform = Instantiate(_entryTemplate, _entryContainer);
            RectTransform entryRectTransfrom = entryTransform.GetComponent<RectTransform>();
            entryRectTransfrom.anchoredPosition = new Vector2(0, - _templateHight * i);
            SetScore(entryTransform, score[i]);
            gameObject.SetActive(true);
            entryRectTransfrom.gameObject.SetActive(true);
            _scores.Add(entryTransform);
        }
    }

    public void Clear()
    {
        _scores.ForEach(f => Destroy(f.gameObject));
        _scores.Clear();
    }

    void SetScore(Transform transfrom, GamerScore score)
    {
        transfrom.Find("gamer_name").GetComponent<Text>().text = score.GamerLogin;
        transfrom.Find("score_decree_00").GetComponent<Text>().text = score.DecreeScore[0].ToString();
        transfrom.Find("score_decree_01").GetComponent<Text>().text = score.DecreeScore[1].ToString();
        transfrom.Find("score_decree_10").GetComponent<Text>().text = score.DecreeScore[2].ToString();
        transfrom.Find("score_decree_11").GetComponent<Text>().text = score.DecreeScore[3].ToString();
        transfrom.Find("score_decree_20").GetComponent<Text>().text = score.DecreeScore[4].ToString();
        transfrom.Find("score_decree_21").GetComponent<Text>().text = score.DecreeScore[5].ToString();
        transfrom.Find("score_decree_30").GetComponent<Text>().text = score.DecreeScore[6].ToString();
        transfrom.Find("score_decree_31").GetComponent<Text>().text = score.DecreeScore[7].ToString();
        transfrom.Find("score_coin_0").GetComponent<Text>().text = score.Coins[0].ToString();
        transfrom.Find("score_coin_1").GetComponent<Text>().text = score.Coins[1].ToString();
        transfrom.Find("score_coin_2").GetComponent<Text>().text = score.Coins[2].ToString();
        transfrom.Find("score_coin_3").GetComponent<Text>().text = score.Coins[3].ToString();
        transfrom.Find("score_monster_0").GetComponent<Text>().text = score.Monsters[0].ToString();
        transfrom.Find("score_monster_1").GetComponent<Text>().text = score.Monsters[1].ToString();
        transfrom.Find("score_monster_2").GetComponent<Text>().text = score.Monsters[2].ToString();
        transfrom.Find("score_monster_3").GetComponent<Text>().text = score.Monsters[3].ToString();
        transfrom.Find("score_total").GetComponent<Text>().text = (score.DecreeScore.Sum() + score.Coins.Sum() - score.Monsters.Sum()).ToString();
    }

    void SetDecreeIcons()
    {
        Transform[] decreeIcons = 
        {
            transform.Find("Icon_decree_00"), // 0
            transform.Find("Icon_decree_01"), // 1
            transform.Find("Icon_decree_10"), // 1
            transform.Find("Icon_decree_11"), // 2
            transform.Find("Icon_decree_20"), // 2
            transform.Find("Icon_decree_21"), // 3
            transform.Find("Icon_decree_30"), // 3
            transform.Find("Icon_decree_31"), // 0
        };
        for (int i = 0; i < decreeIcons.Length; i++)
        {
            decreeIcons[i].GetComponent<Image>().sprite = GameObject.Find(Decrees[(i / 2 + i % 2) % 4].Code.ToString()).GetComponent<SpriteRenderer>().sprite;
            decreeIcons[i].localScale = _decreeIconScale;
        }
    }
}
