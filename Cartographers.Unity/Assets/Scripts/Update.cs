﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Assets.Scripts
{
    class Update
    {
        Dictionary<string, bool> _updates;
        public Update()
        {
            _updates = new Dictionary<string, bool>();
        }

        public void Add(string method)
        {
            if (!_updates.ContainsKey(method))
            {
                _updates[method] = false;
            }
        }
        public void Set(string method, ManualResetEvent reset)
        {
            _updates[method] = true;
            if (_updates.All(a => a.Value))
            {
                reset.Reset();
            }
        }
        public void Reset(ManualResetEvent reset)
        {
            _updates.Select(s => s.Key).ToList().ForEach(f => _updates[f] = false);
            reset.Set();
        }

        public bool this[string method]
            => _updates.ContainsKey(method) && _updates[method];

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in _updates)
            {
                sb.AppendLine(item.Key + ": " + item.Value);
            }
            return sb.ToString();
        }
    }
}
