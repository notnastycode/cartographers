﻿using Cartographers.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    class Matrix
    {
        Dictionary<Point, GameObject> _matrix;
        FieldType[,] _fields;
        int _size;
        public int _offsetX;
        public int _offsetY;
        bool _isRuin;
        float _opacity = 1.0f;

        public Vector2 CurrentPosition { get; set; }
        public Vector2 FromPosition { get; set; }

        public Point Offset => new Point(_offsetX, _offsetY);

        public Matrix(int size)
        {
            _matrix = new Dictionary<Point, GameObject>();
            _fields = new FieldType[size, size];
            _size = size;
            UpdateOffset();
        }

        void UpdateOffset()
        {
            _offsetX = _size == Constants.MapSize ? 0 : Constants.FigureZeroPositionX + (Constants.FigureSize - _fields.GetLength(0)) / 2;
            _offsetY = _size == Constants.MapSize ? 0 : Constants.FigureZeroPositionY + (Constants.FigureSize - _fields.GetLength(1)) / 2;
        }

        public void Rebuild(string name = null)
        {
            for (int x = 0; x < _size; x++)
            {
                for (int y = 0; y < _size; y++)
                {
                    Point point = new Point(x, y);
                    Point pointWithOffset = new Point(x + _offsetX, y + _offsetY);
                    string field = GetFieldAtPoint(point);
                    field = (_isRuin && field == "Ruin") ? "RuinHighlited" : field;
                    if (!_matrix.ContainsKey(point))
                    {
                        _matrix[point] = GameObject.Instantiate(GameObject.Find(field));
                    }
                    SetSprite(_matrix[point], field);
                    _matrix[point].transform.position = Utils.VectorFromPoint(pointWithOffset);
                    _matrix[point].name = field;
                }
            }
        }

        public void Update(FieldType[,] fields, bool isRuinCard = false)
        {
            _fields = fields;
            _isRuin = isRuinCard;
            _opacity = 0;
        }

        public void Update()
        {
            UpdateOffset();
            Rebuild();
        }

        void SetSprite(GameObject go, string name)
        {
            go.GetComponent<SpriteRenderer>().sprite
                        = GameObject.Find(name).GetComponent<SpriteRenderer>().sprite;
        }

        public void Clear() => _matrix.Select(s => s.Value).ToList().ForEach(f => GameObject.Destroy(f));

        string GetFieldAtPoint(Point point)
        {
            string ret;
            if (point.X >= 0 && point.X <_fields.GetLength(0) &&
                point.Y >= 0 && point.Y < _fields.GetLength(1))
            {
                ret = _fields[point.X, point.Y].ToString();
            }
            else
            {
                ret = "None";
            }
            return ret;
        }

        public bool IsEmptyField(Vector2 position)
        {
	        var positionWithoutOffset = new Vector2(position.x - _offsetX, position.y - _offsetY);
	        var point = Utils.VectorToPoint(positionWithoutOffset);
			var field = GetFieldAtPoint(point);
	        return field == "None";
        }

        public void Disintegrate(int count)
        {
            var disintegreatingItems = _matrix.Select(s => s.Key).OrderBy(o => Guid.NewGuid()).Take(count);
            foreach (var item in disintegreatingItems)
            {
                GameObject.Destroy(_matrix[item]);
                _matrix.Remove(item);
            }
        }

        public Vector2 Position
        {
            set
            {
                CurrentPosition = value;
                SetFieldsPosition();
            }
            get
            {
                return CurrentPosition;
            }
        }
        void SetFieldsPosition()
        {
            foreach (KeyValuePair<Point, GameObject> gameObject in _matrix)
            {
                gameObject.Value.transform.position = CurrentPosition +
                   (Utils.VectorFromPoint((Point)gameObject.Key) -
                   FromPosition +
                   new Vector2(_offsetX, _offsetY));
                gameObject.Value.SetLayer(10);
            }
        }

        public void SetTransparency(float amountPerFrame)
        {
            if (_opacity < 1)
            {
                if (amountPerFrame -_opacity < 1) _opacity += amountPerFrame;
                else _opacity = 1;
            }
            _matrix.Select(s => s.Value).ToList().ForEach(f => Utils.SetOpacity(f, _opacity));
        }
    }
}
