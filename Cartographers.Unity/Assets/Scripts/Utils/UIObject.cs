﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
   class UIObject
   {
      public GameObject Object { get; protected set; }

      public void Init(string name, string newName = null)
      {
         GameObject go = Utils.CreateCopyByName(name, newName);
         if (go == null)
         {
            go = new GameObject();
            go.AddComponent<Text>();
         }
         if (go.GetComponent<Canvas>() == null)
         {
            go.AddComponent<Canvas>();
         }
         Object = go;
      }

      public void SetScale(Vector2 scale)
      {
         Object.GetComponent<RectTransform>().localScale = scale;
      }
      public void SetTag(string tag)
      {
         Object.tag = tag;
      }
      public void SetPosition(Vector2 position)
      {
         Object.transform.position = position;
      }

      public void SetFrame(Vector2 frame)
      {
         Object.GetComponent<RectTransform>().sizeDelta = frame;
      }

      public void SetLayer(int layerNumber)
         => Object.SetLayer(layerNumber);
   }
}
