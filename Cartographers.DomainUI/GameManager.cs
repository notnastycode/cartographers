﻿using Cartographers.Domain.DecreeData;
using Cartographers.Domain.ResearchCardData;
using Cartographers.Domain.SeasonData;
using Cartographers.DTO;
using Cartographers.DTO.Db;
using Cartographers.Domain.Game;
using Cartographers.Rabbit;
using System;
using System.Collections.Generic;

namespace Cartographers.DomainUI
{
    public class GameManager : IClientGameManager
	{
		private readonly ResearchCardFactory _rcf = new ResearchCardFactory();
		private readonly DecreeFactory _df = new DecreeFactory();
		private readonly IQueue _queue;
		private ClientGame _game;
		private FigureChanges _fugureChanges;
		public string GamerLogin { get; set; }

		public string SessionId { get; set; }

		public List<string> Gamers { get; private set; }

		public int SeasonTurn => _game.SeasonTurn;

		public Season Season => _game.Season;

		public IDecree[] Decrees => _game.Decrees;

		public IResearchCard ResearchCard => _game.Card;

		private FieldType[,] Figure => _game.Figure.GetFields();

		private FieldType[,] Map => _game.Map.GetFields();

		public bool GameHasEnded => _game.GameHasEnded;

		public bool GameCancelled => _game.GameCancelled;

		public int Coins => _game.Coins;

		public int Monsters => _game.Monsters;

		public int Score => _game.Score;

		public bool TurnAvailable { get; set; }

		public int TurnTimeLeft => _game.TurnTimeLeft;

		public event FigureChanged OnFigureChangeEvent;
		public event MapChanged OnMapChangeEvent;
		public event TurnStarted OnTurnStarted;
		public event GameCreated OnGameCreated;
		public event Action<bool, string> HideWaitingForResponseMessage;
		public event Action<List<GamerScore>, IDecree[]> OnGameEnded;

		public GameManager()
		{
			var settings = new Settings.Builder()
										.SetUser("guest")
										.SetPassword("guest")
										.SetHost("172.30.64.1")
										.SetPort(5672)
										.SetExchange("cartographers_exchange")
										.Build();
			_queue = new Queue(settings);
			InitEmpty();
		}

		public void InitEmpty()
		{
			_game = new ClientGame(_rcf, _df);
			_fugureChanges = new FigureChanges();
			InvokeOnGameChangeEvent();
		}

		public bool CanMove(Point position)
		{
			bool canMove = _game.CanMove(position);
			if (!canMove)
				InvokeOnFigureChangeEvent();
			return canMove;
		}

		public void Rotate()
		{
			if (!TurnAvailable || GameHasEnded) return;
			_game.RotateFigure();
			_fugureChanges.RotateCount += 1;
			if (_fugureChanges.RotateCount >= 4)
				_fugureChanges.RotateCount %= 4;
			InvokeOnFigureChangeEvent();
		}

		public void Reflect()
		{
			if (!TurnAvailable || GameHasEnded) return;
			_game.ReflectFigure();
			if (_fugureChanges.RotateCount % 2 == 0)
				_fugureChanges.IsReflectedHorizontal = !_fugureChanges.IsReflectedHorizontal;
			else
				_fugureChanges.IsReflectedVertical = !_fugureChanges.IsReflectedVertical;
			InvokeOnFigureChangeEvent();
		}

		public void ChangeShape(int index)
		{
			if (!TurnAvailable || GameHasEnded) return;
			_game.SetShapeIndex(index);
			_fugureChanges.ShapeIndex = index;
			InvokeOnFigureChangeEvent();
		}

		public void ChangeType(int index)
		{
			if (!TurnAvailable || GameHasEnded) return;
			_game.SetFieldIndex(index);
			_fugureChanges.FieldIndex = index;
			InvokeOnFigureChangeEvent();
		}

		private void InvokeOnGameChangeEvent()
		{
			if (TurnAvailable)
			{
				InvokeOnFigureChangeEvent();
				OnMapChangeEvent?.Invoke(this, new MapChangedEventArgs { Map = Map });
				OnTurnStarted?.Invoke(this);
			}
			else if(GameHasEnded)
				OnMapChangeEvent?.Invoke(this, new MapChangedEventArgs { Map = Map });
		}

		private void InvokeOnFigureChangeEvent()
		{
			OnFigureChangeEvent?.Invoke(this, new FigureChangedEventArgs { Figure = Figure });
		}

		public void Register(string gamerName, string gamerPassword)
		{
			GamerLogin = gamerName;
			SubscribeAllActions();
			_queue.Publish(new Gamer { Login = gamerName, Password = gamerPassword }, "DbApi.Register");
		}

		public void Login(string gamerName, string gamerPassword)
		{
			GamerLogin = gamerName;
			SubscribeAllActions();
			_queue.Publish(new Gamer { Login = gamerName, Password = gamerPassword }, "DbApi.Login");
		}

		public void CreateGame(SessionSettings settings) => _queue.Publish(settings, "GameApi.CreateGame");

		public void StartGame() => _queue.Publish(new GamerInfo() { SessionId = this.SessionId, GamerLogin = this.GamerLogin }, "GameApi.StartGame");

		public void JoinGame(string sessionId)
		{
			SessionId = sessionId;
			_queue.Publish(new GamerInfo() { SessionId = this.SessionId, GamerLogin = this.GamerLogin }, "GameApi.JoinGame");
		}

		public void Move(Point position)
		{
			_game.Move(position);
			InvokeOnGameChangeEvent();
			TurnAvailable = false;
			var move = new MoveRequest
				{
					SessionId = this.SessionId,
					GamerLogin = this.GamerLogin,
					Move = new MoveDTO { Changes = _fugureChanges, Position = position }
				};
			_queue.Publish(move, "GameApi.MakeMove");
		}

		void SubscribeAllActions()
		{
			_queue.Subscribe<AuthenticationDTO>(result => HideWaitingForResponseMessage?.Invoke(result.Accepted, result.Message), GamerLogin + ".Db", GamerLogin + ".Db");
			
			_queue.Subscribe<List<string>>(result =>
				{
					Gamers = result;
					OnGameCreated?.Invoke(new GameCreatedEventArgs { Password = SessionId, Gamers = Gamers });
				}, GamerLogin + ".GetGamers", GamerLogin + ".GetGamers");

			_queue.Subscribe<GameWithGamerInfo>(result =>
				{
					_game = new ClientGame(result.Game, _rcf, _df);
					SessionId = result.SessionId;
					OnGameCreated?.Invoke(new GameCreatedEventArgs { Password = SessionId, Gamers = Gamers });
				}, GamerLogin + ".CreateGame", GamerLogin + ".CreateGame");
			
			_queue.Subscribe<GameDTO>(result =>
			{
				_game = new ClientGame(result, _rcf, _df);
				_fugureChanges = new FigureChanges();
				TurnAvailable = _game.TurnAvailable && !_game.GameCancelled;
				InvokeOnGameChangeEvent();
				OnTurnStarted?.Invoke(this);
			}, GamerLogin + ".GetGame", GamerLogin + ".GetGame");

			_queue.Subscribe<List<GamerScore>>(result => OnGameEnded?.Invoke(result, Decrees), GamerLogin + ".ShowScore", GamerLogin + ".ShowScore");
		}

		public void Unsubscribe() => _queue.Unsubscribe();
	}
}
