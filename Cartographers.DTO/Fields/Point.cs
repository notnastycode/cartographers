﻿namespace Cartographers.DTO
{
    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point operator +(Point a, Point b)
            => new Point(a.X + b.X, a.Y + b.Y);
        public static Point operator -(Point a, Point b)
            => new Point(a.X - b.X, a.Y - b.Y);
        public static Point operator +(Point a, (int x, int y)p)
            => new Point(a.X + p.x, a.Y + p.y);
        public static Point operator -(Point a, (int x, int y)p)
            => new Point(a.X - p.x, a.Y - p.y);
        public static bool operator ==(Point a, Point b)
            => a.X == b.X && a.Y == b.Y;
        public static bool operator !=(Point a, Point b)
            => !(a == b);
        public static bool operator ==(Point a, (int x, int y) p)
            => a.X == p.x && a.Y == p.y;
        public static bool operator !=(Point a, (int x, int y) p)
            => !(a == p);
        public override int GetHashCode()
            => (X + Y).GetHashCode();
        public override bool Equals(object obj)
            => obj != null &&
               ((obj is Point) || obj.GetType() == typeof((int, int))) &&
               (Point)obj == this;

        public static implicit operator Point((int x, int y)p)
            => new Point(p.x, p.y);
    }
}
