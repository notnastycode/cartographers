﻿namespace Cartographers.DTO
{
	public class MoveDTO
	{
		public Point Position { get; set; }
		public FigureChanges Changes { get; set; }
	}
}
