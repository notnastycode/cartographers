﻿using Cartographers.DTO.Db;

namespace Cartographers.DTO
{
    public class AuthenticationDTO
    {
        public bool Accepted { get; set; }
        public string Message { get; set; }
        public Gamer Gamer { get; set; }

        public AuthenticationDTO(bool accepted, string message, Gamer gamer)
        {
            Accepted = accepted;
            Message = message;
            Gamer = gamer;
        }
        protected AuthenticationDTO() { }
    }
}
