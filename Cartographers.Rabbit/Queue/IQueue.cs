﻿using System;

namespace Cartographers.Rabbit
{
    public interface IQueue
    {
        void Subscribe<T>(Action<T> action, string thisQueue, string readingFromQueue);
        void Publish<T>(T message, string queue);
        void Unsubscribe();
    }
}
